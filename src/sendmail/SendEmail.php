<?php
namespace App\sendmail;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SendEmail implements MailServerConfig
{
    private $host = MailServerConfig::HOST;
    private $username = MailServerConfig::USERNAME;
    private $password = MailServerConfig::PASSWORD;
    private $port = MailServerConfig::PORT;
    private $charset = MailServerConfig::CHARSET;

    public function send($sendTo, $subject, $body, $from = [MailServerConfig::DEFAULT_FROM_EMAIL, MailServerConfig::DEFAULT_FROM_NAME])
    {
        /*
         * Check Format
         */
        if (!$this->checkEmailValueFormatAndType($sendTo)) return $this->responses(false, "The recipient's mailbox format is wrong");
        if (!$this->checkEmailValueFormatAndType($from)) return $this->responses(false, "The sender's mailbox format is wrong");
        if (empty($subject) || empty($body)) return $this->responses(false, "Subject or Content is empty");

        /*
         * Sendmail Class
         */
        $mail = new PHPMailer(true);
        try {
            //Server settings
            //$mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->Host = $this->host;
            $mail->SMTPAuth = true;
            $mail->Username = $this->username;
            $mail->Password = $this->password;
            $mail->SMTPSecure = 'tls';
            $mail->Port = $this->port;
            $mail->CharSet = $this->charset;

            //Recipients
            is_array($from) ? $mail->setFrom($from[0], $from[1]) : $mail->setFrom($from);
            is_array($sendTo) ? $mail->addAddress($sendTo[0], $sendTo[1]) : $mail->addAddress($sendTo);

            //Content
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->send();

            return $this->responses(true, 'ok');
        } catch (Exception $e) {
            return $this->responses(false, $mail->ErrorInfo);
        }
    }

    /*
     * Responses Message
     */
    private function responses($status, $message)
    {
        return ['status' => $status, 'msg' => $message];
    }

    /*
     * Check Format
     */
    protected function checkEmailValueFormatAndType($value)
    {
        $checkValueFormat = is_array($value) ? $this->checkEmailFormat($value[0]) : $this->checkEmailFormat($value);
        return $checkValueFormat ? true : false;
    }
    protected function checkEmailFormat($value)
    {
        $checkEmailFormat = preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $value);
        return (false == $checkEmailFormat) ? false : true;
    }
}