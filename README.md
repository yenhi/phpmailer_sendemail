## PHPMailer - Sendemail method
The method of sendmail by PHPMailer

## Install
* Clone project and run composer install packages
```
$ git clone https://github.com/yenhi/PHPMailer_for_sendemail.git && cd $_
$ composer install
```

## Config
```
cp src/sendmail/MailServerConfig.php.example src/sendmail/MailServerConfig.php && vi src/sendmail/MailServerConfig.php
```

## Example
`public/example.php`